# Configuration scripts

Tmux terminal multiplexer:

    .tmux

Zsh shell with zsh-syntax-highlighting:

    .zsh/
    .zshrc

(Special thanks to: https://github.com/zsh-users/zsh-syntax-highlighting )
